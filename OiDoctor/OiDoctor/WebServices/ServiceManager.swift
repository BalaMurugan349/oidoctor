//
//  ServiceManager.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Alamofire

class ServiceManager: NSObject {

    //MARK:- GET METHOD
    class func getDataFromService(baseurl : String ,serviceName: String, params: [String: Any]?, success:@escaping (Any) -> Void, failue:@escaping (Error) -> Void) {
        
        Alamofire.request("\(baseurl)\(serviceName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = response.result.value {
                success(result)
            } else if response.result.error != nil {
                failue(response.result.error!)
            } else {
                failue(NSError(domain: "", code: 108, userInfo: [NSLocalizedDescriptionKey : "Something went wrong!"]))
            }
        }
    }
    
    //MARK:- POST METHOD
    class func postDataFromService(baseurl : String ,serviceName: String, params: [String: Any]?, success:@escaping (Any) -> Void, failue:@escaping (Error) -> Void) {
        
        Alamofire.request("\(baseurl)\(serviceName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)", method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = response.result.value {
                success(result)
            } else if response.result.error != nil {
                failue(response.result.error!)
            } else {
                failue(NSError(domain: "", code: 108, userInfo: [NSLocalizedDescriptionKey : "Something went wrong!"]))
            }
        }
    }

    class func UploadImageFromService(baseurl : String ,serviceName: String, image : UIImage?, params: [String: Any], success:@escaping (Any) -> Void, failue:@escaping (Error) -> Void) {
        let url = "\(baseurl)\(serviceName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)"
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let imageToUpload = image{
                multipartFormData.append(UIImagePNGRepresentation(imageToUpload)!, withName: "avatar", fileName: ServiceManager.makeFileName(), mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
//                    print("Succesfully uploaded")
//                    if let err = response.error{
//                        onError?(err)
//                        return
//                    }
//                    onCompletion?(nil)
                    if let result = response.result.value {
                        success(result)
                    } else if response.result.error != nil {
                        failue(response.result.error!)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failue(error)
            }
        }

    }

    class func makeFileName() -> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.string(from: NSDate() as Date)
        let randomValue  = arc4random() % 1000
        let fileName : String = "B\(dateString)\(randomValue).png" as String
        return fileName
    }

}
