//
//  UITextfield.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/30/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
    
}
