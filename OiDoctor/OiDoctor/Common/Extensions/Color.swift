//
//  Color.swift
//  GOPS
//
//  Created by P, Balamurugan(AWF) on 27/09/17.
//  Copyright © 2017 P, Balamurugan(AWF). All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public class var buttonBackground: UIColor { return UIColor(red: 42.0/255.0, green: 177.0/255.0, blue: 163.0/255.0, alpha: 1.0) }
    public class var buttonFacebookBackground: UIColor { return UIColor(red: 60.0/255.0, green: 90.0/255.0, blue: 150.0/255.0, alpha: 1.0) }
    public class var orange: UIColor { return UIColor(red: 255.0/255.0, green: 202.0/255.0, blue: 138.0/255.0, alpha: 1.0) }
    public class var alertButtonColor: UIColor { return UIColor(red: 25.0/255.0, green: 76.0/255.0, blue: 166.0/255.0, alpha: 1.0) }



}
