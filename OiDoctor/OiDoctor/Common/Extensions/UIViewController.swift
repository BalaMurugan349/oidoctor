//
//  UIViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func showAlert(with message: String) {
        let alertView = UNAlertView(title: "OiDoctor", message: message)
        alertView.addButton(title: "OK", backgroundColor: UIColor.alertButtonColor) {}
        alertView.show()
    }

}
