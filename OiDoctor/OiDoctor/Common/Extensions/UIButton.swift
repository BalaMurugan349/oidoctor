//
//  UIButton.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    func configure(title : String, titleColor : UIColor, font : UIFont, backgroundColor : UIColor) {
        self.setTitleColor(titleColor, for: UIControlState.normal)
        self.setTitle(title, for: UIControlState.normal)
        self.titleLabel?.font = font
        self.backgroundColor = backgroundColor
        self.clipsToBounds = true
    }
}
