//
//  Dictionary.swift
//  OiDoctor
//
//  Created by Bala Murugan on 2/7/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func checkValueForKey(key: Key) -> Value? {
        // if key not found, replace the nil with
        // the first element of the values collection
        return self[key] ?? nil
        // note, this is still an optional (because the
        // dictionary could be empty)
    }
    
}
