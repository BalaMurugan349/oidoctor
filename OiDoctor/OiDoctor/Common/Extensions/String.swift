//
//  String.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/12/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

extension String{
    //CHECK DEVICE AND GET BACKGROUND IMAGENAME
    func backgroundImageName () -> String{
        return UIDevice().screenType == .iPhoneX ? self + "X" : self
    }
    
    //EMAIL VALIDATION
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.height
    }
    
    //TRIM WHITE SPACE AT TOP AND BOTTOM
    func trimWhiteSpace () -> String{
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var isEmpty : Bool {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }


}
