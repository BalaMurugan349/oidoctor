//
//  LoggedUser.swift
//  OiDoctor
//
//  Created by Bala Murugan on 2/7/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

//CHECK NIL VALUE
func CheckForNull(strToCheck : String?) -> String{
    if let strText = strToCheck {
        return strText
    }
    return "nil"
}

let localeSelected = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"

struct LoggedUser {
    
    static var shared : LoggedUser = LoggedUser()
    var address : String = ""
    var profileImagePath : String = ""
    var city : String = ""
    var country : String = ""
    var dob : String = ""
    var email : String = ""
    var fullName : String = ""
    var gender : String = ""
    var height : String = ""
    var userid : String = ""
    var is_Approved : Bool = false
    var passport : String = ""
    var phoneNumber : String = ""
    var social_id : String = ""
    var token : String = ""
    var userName : String = ""
    var weight : String = ""
    var weightUnit : String = ""
    var isDoctor : Bool = false
    
    
    mutating func storeLoggedUserDetails (dict : NSDictionary){
        address = CheckForNull(strToCheck: dict.checkValueForKey(key: "address") as? String)
        profileImagePath = CheckForNull(strToCheck: dict.checkValueForKey(key: "avatar_path") as? String)
        city = CheckForNull(strToCheck: dict.checkValueForKey(key: "city") as? String)
        country = CheckForNull(strToCheck: dict.checkValueForKey(key: "country") as? String)
        dob = CheckForNull(strToCheck: dict.checkValueForKey(key: "dob") as? String)
        email = CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        fullName = CheckForNull(strToCheck: dict.checkValueForKey(key: "fullname") as? String)
        gender = CheckForNull(strToCheck: dict.checkValueForKey(key: "gender") as? String)
        height = "\(dict["height"] ?? "0")"
        userid = "\(dict["id"] ?? "0")"
        is_Approved = dict["is_approved"] as? Bool ?? false
        passport = CheckForNull(strToCheck: dict.checkValueForKey(key: "passport") as? String)
        phoneNumber = CheckForNull(strToCheck: dict.checkValueForKey(key: "phone") as? String)
        social_id = CheckForNull(strToCheck: dict.checkValueForKey(key: "social_id") as? String)
        token = CheckForNull(strToCheck: dict.checkValueForKey(key: "token") as? String)
        userName = CheckForNull(strToCheck: dict.checkValueForKey(key: "username") as? String)
        weight = "\(dict["weight"] ?? "0")"
        weightUnit = CheckForNull(strToCheck: dict.checkValueForKey(key: "weight_unit") as? String)
        isDoctor = (dict["user_type"] as? NSNumber) == 1

    }
    
    static func saveLoggedUserdetails(dictDetails : NSDictionary){
        let data : NSData = NSKeyedArchiver.archivedData(withRootObject: dictDetails) as NSData
        UserDefaults.standard.set(data, forKey: Constants.loggedUserDetails)
        UserDefaults.standard.synchronize()
    }

}
