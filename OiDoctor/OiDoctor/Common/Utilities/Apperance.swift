//
//  Apperance.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

struct Appearance {
    
    // MARK: Views
    static func registrationNavigationController() -> UINavigationController {
        
        let navigationController = UINavigationController()
        
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isOpaque = true
        navigationController.navigationBar.barStyle = .default
        navigationController.navigationBar.barTintColor = UIColor.white
       // navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        return navigationController
    }
    
    static func homeNavigationController() -> MainNavigationController {
        
        let navigationController = MainNavigationController()
        
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isOpaque = true
        navigationController.navigationBar.barStyle = .default
        navigationController.navigationBar.barTintColor = UIColor.buttonBackground
         navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        return navigationController
    }

}

