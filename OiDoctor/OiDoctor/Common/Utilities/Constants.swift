//
//  Constants.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    #if DEV
    struct API {
    static let baseURL = "http://dev.oidoctor.com.br/api/"
    }
    #else
    struct API {
        static let baseURL = "http://backend.oidoctor.com.br/api/"
    }
    #endif
    
    static let loggedEmail = "LoggedUserEmail"
    static let loggedPassword = "LoggedUserPassword"
    static let loggedUserDetails = "LoggedUserDetails"

}


