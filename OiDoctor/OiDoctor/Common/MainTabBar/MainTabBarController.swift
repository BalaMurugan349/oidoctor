//
//  MainTabBarController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func configureTabBar (){
        // Create Tab one
        
        let vc1 = HomeModule.build()
        let tab1 = self.bindinNavigationController(controller: vc1)
        let tabBarItem1 = UITabBarItem(title: "Tab 1", image: nil, selectedImage: nil)
        tab1.tabBarItem = tabBarItem1
        
        // Create Tab two
        let vc2 = HomeModule.build()
        let tab2 = self.bindinNavigationController(controller: vc2)
        let tabBarItem2 = UITabBarItem(title: "Tab 2", image: nil, selectedImage: nil)
        tab2.tabBarItem = tabBarItem2
        
        // Create Tab three
        let vc3 = HomeModule.build()
        let tab3 = self.bindinNavigationController(controller: vc3)
        let tabBarItem3 = UITabBarItem(title: "Tab 3", image: nil, selectedImage: nil)
        tab3.tabBarItem = tabBarItem3
        
        // Create Tab four
        let vc4 = HomeModule.build()
        let tab4 = self.bindinNavigationController(controller: vc4)
        let tabBarItem4 = UITabBarItem(title: "Tab 4", image: nil, selectedImage: nil)
        tab4.tabBarItem = tabBarItem4
        
        // Create Tab five
        let vc5 = HomeModule.build()
        let tab5 = self.bindinNavigationController(controller: vc5)
        let tabBarItem5 = UITabBarItem(title: "Tab 5", image: nil, selectedImage: nil)
        tab5.tabBarItem = tabBarItem5
        
        self.viewControllers = [tab1, tab2, tab3, tab4, tab5]

    }
    
    func bindinNavigationController (controller : UIViewController) -> MainNavigationController{
        let navVC = Appearance.homeNavigationController()
        navVC.viewControllers = [controller]
        return navVC
    }

}
