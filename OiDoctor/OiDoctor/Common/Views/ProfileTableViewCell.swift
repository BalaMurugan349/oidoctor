//
//  ProfileTableViewCell.swift
//  OiDoctor
//
//  Created by Bala Murugan on 2/1/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class ProfileTableViewCell: UITableViewCell {
    
    let buttonCheckBox = UIButton(type: UIButtonType.custom)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupwith (title : String, subtitle : String , profileImage : UIImage){
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        
        let imgvwBackground = UIImageView(image: #imageLiteral(resourceName: "CellBackground"))
        self.contentView.addSubview(imgvwBackground)
        constrain(imgvwBackground,self.contentView) { (imgvwBackground,contentView) in
            imgvwBackground.leading == contentView.leading + 20
            imgvwBackground.trailing == contentView.trailing - 20
            imgvwBackground.top == contentView.top + 10
            imgvwBackground.bottom == contentView.bottom - 10
        }
        
        let profileImageView = UIImageView(image: profileImage)
        profileImageView.contentMode = .scaleAspectFit
        self.contentView.addSubview(profileImageView)
        constrain(profileImageView,self.contentView) { (profileImageView,contentView) in
            profileImageView.leading == contentView.leading + 35
            profileImageView.width == 35
            profileImageView.height == 50
            profileImageView.centerY == contentView.centerY
        }
        
        buttonCheckBox.setImage(#imageLiteral(resourceName: "ProfileCheckboxUnSelected"), for: UIControlState.normal)
        buttonCheckBox.setImage(#imageLiteral(resourceName: "ProfileCheckboxSelected"), for: UIControlState.selected)
        buttonCheckBox.isUserInteractionEnabled = false
        self.contentView.addSubview(buttonCheckBox)
        constrain(buttonCheckBox,self.contentView) { (buttonCheckBox,contentView) in
            buttonCheckBox.trailing == contentView.trailing - 35
            buttonCheckBox.width == 40
            buttonCheckBox.height == 40
            buttonCheckBox.centerY == contentView.centerY
        }
        
        let labelTitle = UILabel()
        labelTitle.text = title
        labelTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelTitle.textColor = UIColor.black
        labelTitle.adjustsFontSizeToFitWidth = true
        self.contentView.addSubview(labelTitle)
        constrain(labelTitle,self.contentView,buttonCheckBox,profileImageView) { (labelTitle,contentView,buttonCheckBox,profileImageView) in
            labelTitle.leading == profileImageView.trailing + 25
            labelTitle.trailing == buttonCheckBox.leading - 10
            labelTitle.height == 40
            labelTitle.top == contentView.top + 20
        }
        
        let labelSubTitle = UILabel()
        labelSubTitle.text = subtitle
        labelSubTitle.numberOfLines = 0
        labelSubTitle.adjustsFontSizeToFitWidth = true
        labelSubTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 12.0)
        labelSubTitle.textColor = UIColor.darkGray
        self.contentView.addSubview(labelSubTitle)
        constrain(labelSubTitle,self.contentView,buttonCheckBox,profileImageView) { (labelSubTitle,contentView,buttonCheckBox,profileImageView) in
            labelSubTitle.leading == profileImageView.trailing + 25
            labelSubTitle.trailing == buttonCheckBox.leading - 10
            labelSubTitle.bottom == contentView.bottom - 20
            labelSubTitle.top == contentView.top + 50
        }
    }
    
}
