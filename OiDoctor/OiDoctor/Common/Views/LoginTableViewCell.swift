//
//  CustomTextFieldTableViewCell.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class LoginTableViewCell: UITableViewCell {
    
    internal var textfieldCustom : UITextField = UITextField()
    internal var textfieldCity : UITextField = UITextField()
    internal var pickerView : UIPickerView = UIPickerView ()
    internal var datePicker : UIDatePicker = UIDatePicker ()
    internal var arrayValues1 : [String] = []
    internal var arrayValues2 : [String] = []
    internal var textfieldType : PickerType!
    internal var imageViewBackground : UIImageView = UIImageView()
    
    enum PickerType {
        case None
        case Gender
        case DateOfBirth
        case Height
        case Weight
        case Number
        case Address
        case Country
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //SETUP UITEXTFIELD IN TABLEVIEW CELL
    func setupWith(placeholder : String, keyBoardType : UIKeyboardType, isSecureText : Bool, leftImage : UIImage? , pickerType : PickerType)  {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        textfieldType = pickerType
        self.setupBackground()
        self.setupTextfield(textfld: textfieldCustom)
        textfieldCustom.placeholder = placeholder
        textfieldCustom.isSecureTextEntry = isSecureText
        self.setLeftImage(image: leftImage, txtField: textfieldCustom)
        
        switch pickerType {
        case .None:
            textfieldCustom.keyboardType = keyBoardType
            textfieldCustom.keyboardAppearance = .dark
            break
        case .Gender:
            arrayValues1 = ["Male".localize,"Female".localize,"Other".localize,"Secret".localize]
            arrayValues2 = []
            self.setupPickerView(selectedIndex: 0)
            break
        case .DateOfBirth:
            arrayValues1 = []
            arrayValues2 = []
            self.setupDatePicker()
            break
        case .Height:
            var arr = [Int]()
            arr += 30...300
            let array5Multiples = arr.filter { $0 % 5 == 0 }
            arrayValues1 = array5Multiples.map { String($0) }
            arrayValues2 = ["cm"]
            self.setupPickerView(selectedIndex: 12)
            break
        case .Weight:
            var arr = [Int]()
            arr += 1...250
            arrayValues1 = arr.map { String($0) }
            arrayValues2 = ["kg","lb"]
            self.setupPickerView(selectedIndex: 49)
            break
        case .Number:
            textfieldCustom.keyboardType = .numberPad
            textfieldCustom.keyboardAppearance = .dark
            self.setupInputAccessoryView()
            break
        case .Address:
            self.setupTextfield(textfld: textfieldCity)
            self.configureAddressCell()
            break
        case .Country:
            if let fileUrl = Bundle.main.url(forResource: "CountriesList", withExtension: "plist"),
                let data = try? Data(contentsOf: fileUrl) {
                if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String: Any] { // [String: Any] which ever it is
                    arrayValues1 = result![localeSelected] as! [String]
                }
            }
            self.setupPickerView(selectedIndex: 24)
            break

        }
    }
    
    //TEXTFIELD BACKGROUND
    func setupBackground (){
        guard textfieldType == .Address else { return }
        imageViewBackground.image = #imageLiteral(resourceName: "AddressTextfieldBackground")
        self.contentView.addSubview(imageViewBackground)
        constrain(imageViewBackground,self.contentView) { (imgvw,contentView) in
            imgvw.top == contentView.top + 10
            imgvw.leading == contentView.leading + 25
            imgvw.trailing == contentView.trailing - 25
            imgvw.bottom == contentView.bottom - 10
        }
    }
    
    //ADDRESS TABLEVIEW CELL
    func configureAddressCell (){
        self.setLeftImage(image: nil, txtField: textfieldCity)
        textfieldCustom.keyboardAppearance = .dark
        textfieldCity.keyboardAppearance = .dark
        constrain(textfieldCustom,self.contentView,imageViewBackground) { (textfield,contentView,imageViewBackground) in
            textfield.top == contentView.top + 10
            textfield.leading == contentView.leading + 25
            textfield.trailing == contentView.trailing - 25
            textfield.bottom == imageViewBackground.centerY
        }
        
        constrain(textfieldCity,self.contentView,imageViewBackground) { (textfieldCity,contentView,imageViewBackground) in
            textfieldCity.top == imageViewBackground.centerY
            textfieldCity.leading == contentView.leading + 25
            textfieldCity.trailing == contentView.trailing - 25
            textfieldCity.bottom == imageViewBackground.bottom
        }
        textfieldCustom.placeholder = "Your Address".localize
        textfieldCity.placeholder = "Town/ City".localize


    }
    
    //CONFIGURE UITEXTFIELD
    func setupTextfield (textfld : UITextField){
        self.contentView.addSubview(textfld)
        textfld.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        textfld.delegate = self
        guard textfieldType != .Address else { return }
        textfld.background = #imageLiteral(resourceName: "TextfieldBackground")
        constrain(textfieldCustom,self.contentView) { (textfield,contentView) in
            textfield.top == contentView.top + 10
            textfield.leading == contentView.leading + 25
            textfield.trailing == contentView.trailing - 25
            textfield.bottom == contentView.bottom - 10
        }

    }
    
    //LEFT ICON IMAGE
    func setLeftImage (image : UIImage?, txtField : UITextField){
        let width : CGFloat = image == nil ? 20 : 50
        let height = (self.contentView.frame.height - 20)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        let imgvw : UIImageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 30, height: height))
        imgvw.image = image
        imgvw.contentMode = .scaleAspectFit
        paddingView.addSubview(imgvw)
        txtField.leftView = paddingView
        txtField.leftViewMode = .always
    }
    
    //PICKER VIEW
    func setupPickerView (selectedIndex : Int){
        pickerView.delegate = self
        pickerView.dataSource = self
        textfieldCustom.inputView = pickerView
        pickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
        self.setupInputAccessoryView()
    }
    
    //DATE PICKER
    func setupDatePicker (){
        datePicker.timeZone = NSTimeZone.local
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(onDatePickerValueChanged), for: .valueChanged)
        textfieldCustom.inputView = datePicker
        self.setupInputAccessoryView()
        
    }
    
    //INPUT ACCESSORY VIEW - TOOLBAR
    func setupInputAccessoryView (){
        let toolBar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Done".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onDoneButtonPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.sizeToFit()
        textfieldCustom.inputAccessoryView = toolBar
    }
    
    //MARK:- BUTTON ACTIONS
    //DONE BUTTON
    @objc func onDoneButtonPressed (){
        textfieldCustom.resignFirstResponder()
    }
    
    //DATE PICKER ACTION
    @objc func onDatePickerValueChanged (){
        self.setSelectedDateInTextfield(dateSelected: datePicker.date)
    }
    
    func setSelectedDateInTextfield (dateSelected : Date){
        let selectedDate: String = DateFormatters.dateMonthYearFormat.string(from: dateSelected)
        textfieldCustom.text = selectedDate
    }
    
    func getSelectedValueInPicker (){
        var finalValue = arrayValues1[pickerView.selectedRow(inComponent: 0)]
        if textfieldType == .Height || textfieldType == .Weight{
            let value2 = arrayValues2[pickerView.selectedRow(inComponent: 1)]
            finalValue.append(" " + value2)
        }
        textfieldCustom.text = finalValue
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension LoginTableViewCell : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let noPickerType : [LoginTableViewCell.PickerType] = [.None,.DateOfBirth,.Address,.Number]
        if !noPickerType.contains(textfieldType) {
            self.getSelectedValueInPicker()
        }else if textfieldType == .DateOfBirth{
            self.setSelectedDateInTextfield(dateSelected: datePicker.date)
        }
    }
}

//MARK:- UIPICKERVIEW DELEGATE
extension LoginTableViewCell : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return textfieldType == .Height || textfieldType == .Weight ? 2 : 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? arrayValues1.count : arrayValues2.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = component == 0 ? arrayValues1[row] : arrayValues2[row]
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.getSelectedValueInPicker()
    }
    
}
