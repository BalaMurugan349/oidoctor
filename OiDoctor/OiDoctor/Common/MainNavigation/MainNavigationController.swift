//
//  MainNavigationController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController,UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.hidesBackButton = true
        let leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "SideMenu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onSideMenuPressed))
        viewController.navigationItem.leftBarButtonItem = leftBarButtonItem
        
    }
    
    @objc func onSideMenuPressed (){
        self.parent?.menuContainerViewController.toggleLeftSideMenuCompletion { () -> Void in
        }
    }

}
