//
//  RegisterViewModel.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class RegisterViewModel: RegisterViewModelInput {
    
    private let wireframe: RegisterWireframe
    weak var view: RegisterViewModelOutput!
    
    func RegisterTableCellHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6:
            return 70
        case .iPhone6Plus,.iPhoneX:
            return 80
        default:
            return 60
        }
    }
    
    func userPressedRegistrationButton(email: String, password: String, repeatPassword: String, username: String) {
        if username.isEmpty{
            self.view.showAlert(with: "Please enter the username".localize)
        }else if username.count < 7{
            self.view.showAlert(with: "The username must be at least 7 characters.".localize)
        }else if password.isEmpty{
            self.view.showAlert(with: "Please enter the password".localize)
        }else if password.count < 7{
            self.view.showAlert(with: "The Password must be at least 7 characters.".localize)
        }else if password != repeatPassword{
            self.view.showAlert(with: "Password does not match with repeat password".localize)
        }else if email.isEmpty || !email.isValidEmail{
            self.view.showAlert(with: "Please enter the valid email".localize)
        }else{
            SKActivityIndicator.show()
           // let locale = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"
            let loginParams : [String : String] = ["locale" : localeSelected,
                                                "email" : email,
                                                "password" : password,
                                                "username" : username]
            self.signupWithApi(params: loginParams)
            
        }
    }
    
    func didFinishEnteringFullName(fullName : String) {
        if fullName.isEmpty{
            self.view.showAlert(with: "Please enter your Full Name".localize)
        }else if fullName.count < 7{
            self.view.showAlert(with: "The Fullname must be at least 7 characters.".localize)
        }else{
            LoggedUser.shared.fullName = fullName
            self.wireframe.gotoChooseProfilePage()
        }
    }
    
    func didFinishSelectingProfileType (isDoctor : Bool){
        LoggedUser.shared.isDoctor = isDoctor
        isDoctor ? self.updateProfileWithApi(imageProfile: nil) : self.wireframe.gotoPatientDetailsPage()
    }
    
    func didFinishEnteringPatientDetails(gender : String,birthday : String, height : String, weight : String) {
        if gender.isEmpty{
            self.view.showAlert(with: "Please enter your Gender".localize)
        }else if birthday.isEmpty{
            self.view.showAlert(with: "Please enter your Birthday".localize)
        }else if height.isEmpty{
            self.view.showAlert(with: "Please enter your Height".localize)
        }else if weight.isEmpty{
            self.view.showAlert(with: "Please enter your Weight".localize)
        }else{
            LoggedUser.shared.gender = gender
            LoggedUser.shared.dob = birthday
            let heightArray = height.components(separatedBy: " ")
            LoggedUser.shared.height = heightArray[0]
            let weightArray = weight.components(separatedBy: " ")
            LoggedUser.shared.weight = weightArray[0]
            LoggedUser.shared.weightUnit = weightArray[1]
            self.wireframe.goToUserProfilePage()
        }
    }
    func userPressedSkipButton() {
        self.wireframe.didLogin()
    }
    
    func didFinishUserProfile(passport : String, address : String, city : String, country : String, phoneNumber : String, profileImage : UIImage?) {
        if passport.isEmpty{
            self.view.showAlert(with: "Please enter your Passport Number".localize)
        }else if address.isEmpty{
            self.view.showAlert(with: "Please enter your Address".localize)
        }else if city.isEmpty{
            self.view.showAlert(with: "Please enter your Town/City".localize)
        }else if country.isEmpty{
            self.view.showAlert(with: "Please enter your Country".localize)
        }else if !phoneNumber.isEmpty && phoneNumber.count < 10{
            self.view.showAlert(with: "Please enter the valid phone number".localize)
        }else{
            LoggedUser.shared.passport = passport
            LoggedUser.shared.address = address
            LoggedUser.shared.city = city
            LoggedUser.shared.country = country
            LoggedUser.shared.phoneNumber = phoneNumber.isEmpty ? "" : phoneNumber
            self.updateProfileWithApi(imageProfile: profileImage)
        }
    }
    
    init(wireframe: RegisterWireframe) {
        self.wireframe = wireframe
        
    }
    
    //SIGNUP WITH API
    func signupWithApi(params : [String : String])  {
        print("Params = \(params)")
        ServiceManager.postDataFromService(baseurl: Constants.API.baseURL, serviceName: "signup", params: params, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.showAlert(with: "Registration Failed! Please try again".localize)
                }else{
                    (self.view as! RegisterTableViewController).delegate.didFinishedRegistration(email: params["email"]!, password: params["password"]!)
                    (self.view as! RegisterTableViewController).navigationController?.popViewController(animated: true)
                }
            }
            print(result)
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.showAlert(with: error.localizedDescription)
        }
    }
    
    //UPDATE PROFILE DETAILS
    func updateProfileWithApi(imageProfile : UIImage?) {
        SKActivityIndicator.show()
       // let locale = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"
        let params : [String : String] = ["id" : LoggedUser.shared.userid,
                                          "fullname" : LoggedUser.shared.fullName,
                                          "gender" : LoggedUser.shared.gender,
                                          "dob" : LoggedUser.shared.dob,
                                          "passport" : LoggedUser.shared.passport,
                                          "address" : LoggedUser.shared.address,
                                          "city" : LoggedUser.shared.city,
                                          "country" : LoggedUser.shared.country,
                                          "phone" : LoggedUser.shared.phoneNumber,
                                          "height" : LoggedUser.shared.height,
                                          "weight" : LoggedUser.shared.weight,
                                          "token" : LoggedUser.shared.token,
                                          "locale" : localeSelected,
                                          "user_type" : LoggedUser.shared.isDoctor ? "1" : "0",
                                          "weight_unit" : LoggedUser.shared.weightUnit]
        print("Params = \(params)")
        
        ServiceManager.UploadImageFromService(baseurl: Constants.API.baseURL, serviceName: "updateprofile", image: imageProfile, params: params, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.showAlert(with: "Profile Updation Failed! Please try again".localize)
                }else{
                    LoggedUser.saveLoggedUserdetails(dictDetails: resultDict["data"] as! NSDictionary)
                    LoggedUser.shared.storeLoggedUserDetails(dict: resultDict["data"] as! NSDictionary)
                    self.wireframe.didLogin()
                }
            }
            print(result)
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.showAlert(with: error.localizedDescription)
        }
        
//        ServiceManager.postDataFromService(baseurl: Constants.API.baseURL, serviceName: "updateprofile", params: params, success: { (result) in
//            SKActivityIndicator.dismiss()
//            if let resultDict = result as? [String : Any],
//                let status = resultDict["status"] as? NSNumber{
//                if status == 0{
//                    self.view.showAlert(with: "Profile Updation Failed! Please try again".localize)
//                }else{
//                    self.wireframe.didLogin()
//                }
//            }
//            print(result)
//        }) { (error) in
//            SKActivityIndicator.dismiss()
//            self.view.showAlert(with: error.localizedDescription)
//        }

    }
    
}
