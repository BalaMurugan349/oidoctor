//
//  RegisterModule.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

struct RegisterModule {
    
    //REGISTRATION PAGE
    static func build(fromVC : LoginTableViewController) -> RegisterTableViewController {
        let wireframe = RegisterWireframe()
        let viewModel = RegisterViewModel(wireframe: wireframe)
        let viewController = RegisterTableViewController(viewModel: viewModel)
        viewModel.view = viewController as RegisterViewModelOutput
        viewController.delegate = fromVC
        wireframe.viewController = viewController
        return viewController
    }
    
    //REGISTRATION - FULLNAME PAGE
    static func buildFullname() -> FullNameTableViewController {
        let wireframe = RegisterWireframe()
        let viewModel = RegisterViewModel(wireframe: wireframe)
        let viewController = FullNameTableViewController(viewModel: viewModel)
        viewModel.view = viewController as RegisterViewModelOutput
        wireframe.viewController = viewController
        return viewController
    }
    
    //REGISTRATION - PROFILE TYPE PAGE
    static func buildChooseProfile() -> ChooseProfileTableViewController {
        let wireframe = RegisterWireframe()
        let viewModel = RegisterViewModel(wireframe: wireframe)
        let viewController = ChooseProfileTableViewController(viewModel: viewModel)
        viewModel.view = viewController as RegisterViewModelOutput
        wireframe.viewController = viewController
        return viewController
    }

    //REGISTRATION - PATIENT DETAILS PAGE
    static func buildPatientDetails() -> PatientDetailsTableViewController {
        let wireframe = RegisterWireframe()
        let viewModel = RegisterViewModel(wireframe: wireframe)
        let viewController = PatientDetailsTableViewController(viewModel: viewModel)
        viewModel.view = viewController as RegisterViewModelOutput
        wireframe.viewController = viewController
        return viewController
    }

    //USER PROFILE - FIRST PAGE
    static func buildUserProfile() -> UserProfileTableViewController {
        let wireframe = RegisterWireframe()
        let viewModel = RegisterViewModel(wireframe: wireframe)
        let viewController = UserProfileTableViewController(viewModel: viewModel)
        viewModel.view = viewController as RegisterViewModelOutput
        wireframe.viewController = viewController
        return viewController
    }


}
