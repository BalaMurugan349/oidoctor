//
//  RegisterWireframe.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class RegisterWireframe {
    
    weak var viewController: RegisterTableViewController!
    
    
    //USER PROFILE PAGE
    func goToUserProfilePage (){
        let userProfileVC = RegisterModule.buildUserProfile()
        viewController.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    //PROFILE TYPE PAGE
    func gotoChooseProfilePage (){
        let profileVC = RegisterModule.buildChooseProfile()
        viewController.navigationController?.pushViewController(profileVC, animated: true)

    }
    
    //PATIENT DETAILS PAGE
    func gotoPatientDetailsPage (){
        let patientVC = RegisterModule.buildPatientDetails()
        viewController.navigationController?.pushViewController(patientVC, animated: true)

    }
    
    //HOME
    func didLogin () {
        (UIApplication.shared.delegate as! AppDelegate).didLogin()
    }
}
