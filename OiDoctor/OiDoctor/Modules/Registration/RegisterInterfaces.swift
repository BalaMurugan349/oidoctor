//
//  RegisterInterfaces.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterViewModelOutput: class {
    func showAlert(with message: String)
    
}

protocol RegisterViewModelInput: class {
    func RegisterTableCellHeight () -> CGFloat
    func userPressedRegistrationButton(email : String, password : String, repeatPassword : String , username : String)
    func didFinishEnteringFullName(fullName : String)
    func didFinishSelectingProfileType (isDoctor : Bool)
    func didFinishEnteringPatientDetails(gender : String,birthday : String, height : String, weight : String)
    func didFinishUserProfile(passport : String, address : String, city : String, country : String, phoneNumber : String, profileImage : UIImage?)
    func userPressedSkipButton ()
}
