//
//  FullNameTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/13/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class FullNameTableViewController: RegisterTableViewController {
    
    private let fullNameCell : LoginTableViewCell = LoginTableViewCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        let skipButton = UIBarButtonItem(title: "Skip".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onSkipButtonPressed))
        self.navigationItem.rightBarButtonItem = skipButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //SETBACKGROUND IMAGE
    override func setBackgroundImage (){
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "ProfileBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        
    }
    
    //TABLE HEADER VIEW
    override func setupTableHeaderView() {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight + 70))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelNameText : UILabel = UILabel()
        labelNameText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0)
        labelNameText.textColor = UIColor.white
        labelNameText.text = "WHAT IS YOUR NAME?".localize
        viewHeader.addSubview(labelNameText)
        constrain(labelNameText,viewHeader) { (labelNameText,viewHeader) in
            labelNameText.leading == viewHeader.leading + 30
            labelNameText.trailing == viewHeader.trailing - 30
            labelNameText.bottom == viewHeader.bottom - 20
            labelNameText.height == 50
        }
        
        let labelWelcomeText : UILabel = UILabel()
        labelWelcomeText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelWelcomeText.numberOfLines = 0
        labelWelcomeText.textColor = UIColor.white
        labelWelcomeText.textAlignment = .center
        labelWelcomeText.text = "FullName Welcome Text".localize
        viewHeader.addSubview(labelWelcomeText)
        constrain(labelWelcomeText,labelNameText,viewHeader) { (labelWelcomeText,labelNameText,viewHeader) in
            labelWelcomeText.leading == viewHeader.leading + 30
            labelWelcomeText.trailing == viewHeader.trailing - 30
            labelWelcomeText.bottom == labelNameText.top - 10
            labelWelcomeText.top == viewHeader.top + 10
        }
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells() {
        fullNameCell.setupWith(placeholder: "Your Full Name".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: #imageLiteral(resourceName: "Username"), pickerType: .None)
        arrayTableViewCells.append(fullNameCell)
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView() {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 100))
        self.tableView.tableFooterView = viewFooter
        
        let buttonHeight = self.viewModel.RegisterTableCellHeight() - 20
        let buttonSubmit = UIButton(type: UIButtonType.custom)
        buttonSubmit.configure(title: "NEXT".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonSubmit.addTarget(self, action: #selector(onNextButtonPressed), for: UIControlEvents.touchUpInside)
        buttonSubmit.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        viewFooter.addSubview(buttonSubmit)
        constrain(buttonSubmit,viewFooter) { (buttonSubmit,view) in
            buttonSubmit.top == view.top + 20
            buttonSubmit.leading == view.leading + 30
            buttonSubmit.trailing == view.trailing - 30
            buttonSubmit.height == buttonHeight
        }
    }
    
    //MARK:- BUTTON ACTIONS
    //SUBMIT BUTTON ACTION
    @objc func onNextButtonPressed (){
        self.viewModel.didFinishEnteringFullName(fullName: fullNameCell.textfieldCustom.text!)
    }
    
    //SKIP BUTTON ACTION
    @objc func onSkipButtonPressed (){
        self.viewModel.userPressedSkipButton()
    }
}
