//
//  RegisterTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography
import BonMot

class RegisterTableViewController: UITableViewController,RegisterViewModelOutput {
    
    internal let viewModel: RegisterViewModelInput
    internal var arrayTableViewCells : [LoginTableViewCell] = []
    internal let tableHeaderHeight : CGFloat = UIDevice().screenHeight * 0.30
    private let usernameCell : LoginTableViewCell = LoginTableViewCell()
    private let passwordCell : LoginTableViewCell = LoginTableViewCell()
    private let passwordRepeatCell : LoginTableViewCell = LoginTableViewCell()
    private let emailCell : LoginTableViewCell = LoginTableViewCell()
    internal var delegate : registrationDelegate!
    
    init(viewModel: RegisterViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
        self.setBackgroundImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- SETUP UI
    func setupView (){
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTableHeaderView()
        self.setupTableViewCells()
        self.setUpFooterView()
    }
    
    //SETBACKGROUND IMAGE
    func setBackgroundImage (){
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "SignUpBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        
    }
    
    //TABLE HEADER VIEW
    func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        
        let buttonBack = UIButton(type: UIButtonType.custom)
        buttonBack.configure(title: "", titleColor: UIColor.white, font: UIFont.systemFont(ofSize: 25), backgroundColor: UIColor.clear)
        buttonBack.setImage(#imageLiteral(resourceName: "BackButton"), for: UIControlState.normal)
        buttonBack.addTarget(self, action: #selector(onBackButtonPressed), for: UIControlEvents.touchUpInside)
        buttonBack.tintColor = UIColor.white
        viewHeader.addSubview(buttonBack)
        constrain(buttonBack,viewHeader) { (buttonBack,viewHeader) in
            buttonBack.top == viewHeader.top
            buttonBack.leading == viewHeader.leading
            buttonBack.width == 50
            buttonBack.height == 50
        }
        
        let imgvwLogo = UIImageView(image: #imageLiteral(resourceName: "OiDoctorText"))
        imgvwLogo.contentMode = .scaleAspectFit
        viewHeader.addSubview(imgvwLogo)
        constrain(imgvwLogo,viewHeader) { (imgvwLogo,viewHeader) in
            imgvwLogo.width == UIDevice().screenWidth/2 - 20
            imgvwLogo.center == viewHeader.center
            imgvwLogo.height == 40
        }
        
        let labelHeading : UILabel = UILabel()
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: UIDevice().screenType == .iPhone5 ? 30 : 35)
        labelHeading.textColor = UIColor.white
        labelHeading.text = "SIGN UP".localize
        labelHeading.textAlignment = .center
        viewHeader.addSubview(labelHeading)
        constrain(labelHeading,imgvwLogo,viewHeader) { (labelHeading,imgvwLogo,viewHeader) in
            labelHeading.leading == viewHeader.leading + 30
            labelHeading.trailing == viewHeader.trailing - 30
            labelHeading.top == imgvwLogo.bottom + 20
            labelHeading.height == 50
        }
        
        
        
        self.tableView.tableHeaderView = viewHeader
    }
    
    //TABLE VIEW CELLS
    func setupTableViewCells (){
        usernameCell.setupWith(placeholder: "Choose an Username".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: #imageLiteral(resourceName: "Username"), pickerType: .None)
        arrayTableViewCells.append(usernameCell)
        
        passwordCell.setupWith(placeholder: "Choose your Password".localize, keyBoardType: UIKeyboardType.default, isSecureText: true, leftImage: #imageLiteral(resourceName: "Password") ,pickerType: .None)
        arrayTableViewCells.append(passwordCell)
        
        passwordRepeatCell.setupWith(placeholder: "Repeat your Password".localize, keyBoardType: UIKeyboardType.default, isSecureText: true, leftImage: #imageLiteral(resourceName: "Password") ,pickerType: .None)
        arrayTableViewCells.append(passwordRepeatCell)
        
        emailCell.setupWith(placeholder: "Your Email Address".localize, keyBoardType: UIKeyboardType.emailAddress, isSecureText: false, leftImage: #imageLiteral(resourceName: "Username"), pickerType: .None)
        arrayTableViewCells.append(emailCell)
        
        self.tableView.reloadData()
    }
    
    //TABLE FOOTER VIEW
    func setUpFooterView (){
        let height = UIDevice().screenHeight - (self.viewModel.RegisterTableCellHeight() * CGFloat(arrayTableViewCells.count)) - tableHeaderHeight - UIDevice().statusBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //REGISTRATION BUTTON
        let buttonHeight = self.viewModel.RegisterTableCellHeight() - 20
        let topSpace = (height - (buttonHeight * 2))/3
        let buttonRegister = UIButton(type: UIButtonType.custom)
        buttonRegister.configure(title: "REGISTRATION".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonRegister.addTarget(self, action: #selector(onRegisterButtonPressed), for: UIControlEvents.touchUpInside)
        buttonRegister.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        viewFooter.addSubview(buttonRegister)
        constrain(buttonRegister,viewFooter) { (buttonRegister,view) in
            buttonRegister.top == view.top + topSpace
            buttonRegister.leading == view.leading + 30
            buttonRegister.trailing == view.trailing - 30
            buttonRegister.height == buttonHeight
        }
        
        //TERMS AND CONDITIONS
        let fontSize : CGFloat = UIDevice().screenType == .iPhone5 ? Localization.sharedInstance.getCurrentLanguage().name == "Brazil" ? 11.0 : 12.0 : 13.0
        let baseStyle = StringStyle(
            .color(.white),
            .font(Font.make(FontName.varelaRound, weight: FontWeight.normal, size: fontSize)),
            .alignment(.center)
        )
        let termsTitle = NSMutableAttributedString()
        let termsColor = baseStyle.byAdding(.color(.orange))
        termsTitle.append("By clicking <<Registration>> you agree to our \n".localize.styled(with: baseStyle))
        termsTitle.append("Terms of Service.".localize.styled(with: termsColor))
        
        let termsButton = UIButton(type: UIButtonType.custom)
        termsButton.configure(title: "", titleColor: UIColor.white, font: UIFont.systemFont(ofSize: 15), backgroundColor: UIColor.clear)
        termsButton.setAttributedTitle(termsTitle, for: UIControlState.normal)
        termsButton.titleLabel?.lineBreakMode = .byWordWrapping
        termsButton.titleLabel?.numberOfLines = 0
        viewFooter.addSubview(termsButton)
        constrain(termsButton,buttonRegister,viewFooter) { (termsButton,buttonRegister,view) in
            termsButton.top == buttonRegister.bottom + topSpace + 15
            termsButton.leading == view.leading + 10
            termsButton.trailing == view.trailing - 10
            termsButton.height == buttonHeight
        }
    }
    
    //MARK:- BUTTON ACTIONS
    //BACK BUTTON
    @objc func onBackButtonPressed (){
        self.navigationController?.popViewController(animated: true)
    }
    
    //REGISTRATION BUTTON
    @objc func onRegisterButtonPressed (){
        self.viewModel.userPressedRegistrationButton(email: emailCell.textfieldCustom.text! , password: passwordCell.textfieldCustom.text!, repeatPassword: passwordRepeatCell.textfieldCustom.text!, username: usernameCell.textfieldCustom.text!)
    }
    
    //MARK:- UITABLEVIEW DELEGATE
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.RegisterTableCellHeight()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayTableViewCells[indexPath.row]
    }
    
    
}
