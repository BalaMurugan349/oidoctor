//
//  UserProfileFirstTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/15/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class UserProfileTableViewController: RegisterTableViewController {
    
    private var imagePicker : UIImagePickerController!
    private let buttonUserPic = UIButton(type: UIButtonType.custom)
    private let passportCell : LoginTableViewCell = LoginTableViewCell()
    private let addressCell : LoginTableViewCell = LoginTableViewCell()
    private let countryCell : LoginTableViewCell = LoginTableViewCell()
    private let telephoneCell : LoginTableViewCell = LoginTableViewCell()
    private var profileImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated:true)
        let skipButton = UIBarButtonItem(title: "Skip".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onSkipButtonPressed))
        self.navigationItem.rightBarButtonItem = skipButton
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), landscapeImagePhone: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //SETBACKGROUND IMAGE
    override func setBackgroundImage (){
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "ProfileBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        
    }
    
    //TABLE HEADER VIEW
    override func setupTableHeaderView() {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 180))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelLastStep : UILabel = UILabel()
        labelLastStep.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelLastStep.textColor = UIColor.white
        labelLastStep.text = "LAST STEP!".localize
        viewHeader.addSubview(labelLastStep)
        constrain(labelLastStep,viewHeader) { (labelLastStep,viewHeader) in
            labelLastStep.leading == viewHeader.leading + 30
            labelLastStep.trailing == viewHeader.trailing - 30
            labelLastStep.height == 40
            labelLastStep.top == viewHeader.top + 10
        }
        
        buttonUserPic.configure(title: "", titleColor: UIColor.white, font: UIFont.systemFont(ofSize: 25), backgroundColor: UIColor.clear)
        buttonUserPic.setImage(#imageLiteral(resourceName: "Camera"), for: UIControlState.normal)
        buttonUserPic.imageView?.contentMode = .scaleAspectFill
        buttonUserPic.addTarget(self, action: #selector(onUserProfilePicSelected), for: UIControlEvents.touchUpInside)
        buttonUserPic.layer.cornerRadius = 30.0
        buttonUserPic.clipsToBounds = true
        viewHeader.addSubview(buttonUserPic)
        constrain(buttonUserPic,viewHeader) { (buttonUserPic,viewHeader) in
            buttonUserPic.centerX == viewHeader.centerX
            buttonUserPic.bottom == viewHeader.bottom - 10
            buttonUserPic.width == 100
            buttonUserPic.height == 100
        }
        
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells() {
        passportCell.setupWith(placeholder: "Your ID / Passport #".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .None)
        arrayTableViewCells.append(passportCell)
        
        addressCell.setupWith(placeholder: "Your Address".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Address)
        arrayTableViewCells.append(addressCell)
        
        countryCell.setupWith(placeholder: "Your Country".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Country)
        arrayTableViewCells.append(countryCell)
        
        telephoneCell.setupWith(placeholder: "Your Telefone (optional)".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Number)
        arrayTableViewCells.append(telephoneCell)
        
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView() {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 100))
        self.tableView.tableFooterView = viewFooter
        
        let buttonHeight = self.viewModel.RegisterTableCellHeight() - 20
        let buttonSubmit = UIButton(type: UIButtonType.custom)
        buttonSubmit.configure(title: "DONE".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonSubmit.addTarget(self, action: #selector(onDoneButtonPressed), for: UIControlEvents.touchUpInside)
        buttonSubmit.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        viewFooter.addSubview(buttonSubmit)
        constrain(buttonSubmit,viewFooter) { (buttonSubmit,view) in
            buttonSubmit.top == view.top + 20
            buttonSubmit.leading == view.leading + 30
            buttonSubmit.trailing == view.trailing - 30
            buttonSubmit.height == buttonHeight
        }
        
    }
    
    //MARK:- BUTTON ACTIONS
    //SKIP BUTTON
    @objc func onSkipButtonPressed (){
        self.viewModel.userPressedSkipButton()
    }
    
    //PROFILE PICTURE
    @objc func onUserProfilePicSelected (){
        let actionSheet : UIAlertController = UIAlertController(title: "Choose".localize, message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel".localize, style: .cancel) { action -> Void in
        }
        actionSheet.addAction(cancelActionButton)
        let photo: UIAlertAction = UIAlertAction(title: "Upload from photo album".localize, style: .default)
        { action -> Void in
            self.showImagePicker(source: .photoLibrary)
        }
        actionSheet.addAction(photo)
        
        let camera: UIAlertAction = UIAlertAction(title: "Capture from camera".localize, style: .default)
        { action -> Void in
            self.showImagePicker(source: .camera)
        }
        actionSheet.addAction(camera)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    //MARK:- SHOW IMAGE PICKER
    func showImagePicker (source : UIImagePickerControllerSourceType){
        imagePicker = imagePicker == nil ? UIImagePickerController() : imagePicker
        imagePicker.sourceType = source
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //DONE BUTTON
    @objc func onDoneButtonPressed (){
        self.viewModel.didFinishUserProfile(passport: passportCell.textfieldCustom.text!, address: addressCell.textfieldCustom.text!, city: addressCell.textfieldCity.text!, country: countryCell.textfieldCustom.text!, phoneNumber: telephoneCell.textfieldCustom.text!, profileImage: profileImage)
    }
    
    //TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 1 ? (self.viewModel.RegisterTableCellHeight() * 2) : self.viewModel.RegisterTableCellHeight()
    }
    
}

//MARK:- UIIMAGE PICKER CONTROLLER DELEGATE
extension UserProfileTableViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        OperationQueue.main.addOperation {
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.profileImage = image
                self.buttonUserPic.setImage(image, for: UIControlState.normal)
            } else{
                print("Something went wrong")
            }
        }
        
    }
}

