//
//  ChooseProfileTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/31/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class ChooseProfileTableViewController: RegisterTableViewController {
    
    internal var arrayProfileTableViewCells : [ProfileTableViewCell] = []
    private let doctorCell : ProfileTableViewCell = ProfileTableViewCell()
    private let patientCell : ProfileTableViewCell = ProfileTableViewCell()
    private var isDoctor : Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        let skipButton = UIBarButtonItem(title: "Skip".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onSkipButtonPressed))
        self.navigationItem.rightBarButtonItem = skipButton
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), landscapeImagePhone: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //SETBACKGROUND IMAGE
    override func setBackgroundImage (){
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "ProfileBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        
    }
    
    //TABLE HEADER VIEW
    override func setupTableHeaderView() {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 80))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelProfileText : UILabel = UILabel()
        labelProfileText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0)
        labelProfileText.textColor = UIColor.white
        labelProfileText.text = "WHAT IS YOUR PROFILE?".localize
        viewHeader.addSubview(labelProfileText)
        constrain(labelProfileText,viewHeader) { (labelProfileText,viewHeader) in
            labelProfileText.leading == viewHeader.leading + 30
            labelProfileText.trailing == viewHeader.trailing - 30
            labelProfileText.bottom == viewHeader.bottom - 10
            labelProfileText.height == 50
        }
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells() {
        doctorCell.setupwith(title: "Healthcare Professional".localize, subtitle: "Doctor Subtitle".localize, profileImage: #imageLiteral(resourceName: "Doctor"))
        arrayProfileTableViewCells.append(doctorCell)
        doctorCell.buttonCheckBox.isSelected = true
        patientCell.setupwith(title: "Personal".localize, subtitle: "Patient Subtitle".localize, profileImage: #imageLiteral(resourceName: "Patient"))
        arrayProfileTableViewCells.append(patientCell)
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView() {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 100))
        self.tableView.tableFooterView = viewFooter
        
        let buttonHeight = self.viewModel.RegisterTableCellHeight() - 20
        let buttonSubmit = UIButton(type: UIButtonType.custom)
        buttonSubmit.configure(title: "NEXT".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonSubmit.addTarget(self, action: #selector(onNextButtonPressed), for: UIControlEvents.touchUpInside)
        buttonSubmit.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        viewFooter.addSubview(buttonSubmit)
        constrain(buttonSubmit,viewFooter) { (buttonSubmit,view) in
            buttonSubmit.top == view.top + 10
            buttonSubmit.leading == view.leading + 30
            buttonSubmit.trailing == view.trailing - 30
            buttonSubmit.height == buttonHeight
        }
    }
    
    //MARK:- BUTTON ACTION
    //SKIP BUTTON ACTION
    @objc func onSkipButtonPressed (){
        self.viewModel.userPressedSkipButton()
    }
    
    //NEXT BUTTON ACTION
    @objc func onNextButtonPressed (){
        self.viewModel.didFinishSelectingProfileType(isDoctor: isDoctor)
    }
    
    //MARK:- UITABLEVIEW DELEGATE
    func numberOfSectionrs(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayProfileTableViewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayProfileTableViewCells[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        doctorCell.buttonCheckBox.isSelected = indexPath.row == 0
        patientCell.buttonCheckBox.isSelected = indexPath.row == 1
        isDoctor = indexPath.row == 0
        self.tableView.reloadData()
    }
    
    
}
