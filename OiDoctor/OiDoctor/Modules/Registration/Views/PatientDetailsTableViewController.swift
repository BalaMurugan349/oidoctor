//
//  PatientDetailsTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/31/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class PatientDetailsTableViewController: RegisterTableViewController {
    
    private let genderCell : LoginTableViewCell = LoginTableViewCell()
    private let dobCell : LoginTableViewCell = LoginTableViewCell()
    private let heightCell : LoginTableViewCell = LoginTableViewCell()
    private let weightCell : LoginTableViewCell = LoginTableViewCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        let skipButton = UIBarButtonItem(title: "Skip".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onSkipButtonPressed))
        self.navigationItem.rightBarButtonItem = skipButton
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), landscapeImagePhone: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //SETBACKGROUND IMAGE
    override func setBackgroundImage (){
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "ProfileBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        
    }
    
    //TABLE HEADER VIEW
    override func setupTableHeaderView() {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelWelcomeText : UILabel = UILabel()
        labelWelcomeText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelWelcomeText.numberOfLines = 0
        labelWelcomeText.textColor = UIColor.white
        labelWelcomeText.textAlignment = .center
        labelWelcomeText.text = "FullName Welcome Text".localize
        viewHeader.addSubview(labelWelcomeText)
        constrain(labelWelcomeText,viewHeader) { (labelWelcomeText,viewHeader) in
            labelWelcomeText.leading == viewHeader.leading + 30
            labelWelcomeText.trailing == viewHeader.trailing - 30
            labelWelcomeText.bottom == viewHeader.bottom - 10
            labelWelcomeText.top == viewHeader.top + 10
        }
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells() {
        
        genderCell.setupWith(placeholder: "Your Gender".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Gender)
        arrayTableViewCells.append(genderCell)
        
        dobCell.setupWith(placeholder: "Your Birthday".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .DateOfBirth)
        arrayTableViewCells.append(dobCell)
        
        heightCell.setupWith(placeholder: "Your Height".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Height)
        arrayTableViewCells.append(heightCell)
        
        weightCell.setupWith(placeholder: "Your Weight".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: nil, pickerType: .Weight)
        arrayTableViewCells.append(weightCell)
        
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView() {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 100))
        self.tableView.tableFooterView = viewFooter
        
        let buttonHeight = self.viewModel.RegisterTableCellHeight() - 20
        let buttonSubmit = UIButton(type: UIButtonType.custom)
        buttonSubmit.configure(title: "NEXT".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonSubmit.addTarget(self, action: #selector(onNextButtonPressed), for: UIControlEvents.touchUpInside)
        buttonSubmit.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        viewFooter.addSubview(buttonSubmit)
        constrain(buttonSubmit,viewFooter) { (buttonSubmit,view) in
            buttonSubmit.top == view.top + 20
            buttonSubmit.leading == view.leading + 30
            buttonSubmit.trailing == view.trailing - 30
            buttonSubmit.height == buttonHeight
        }
    }
    
    //MARK:- BUTTON ACTIONS
    //SUBMIT BUTTON ACTION
    @objc func onNextButtonPressed (){
        self.viewModel.didFinishEnteringPatientDetails(gender: genderCell.textfieldCustom.text!, birthday: dobCell.textfieldCustom.text!, height: heightCell.textfieldCustom.text!, weight: weightCell.textfieldCustom.text!)
    }
    
    //SKIP BUTTON ACTION
    @objc func onSkipButtonPressed (){
        self.viewModel.userPressedSkipButton()
    }
}

