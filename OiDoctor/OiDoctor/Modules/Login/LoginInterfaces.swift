//
//  LoginInterfaces.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewModelOutput: class {
    func showAlert(with message: String)
}

protocol LoginViewModelInput: class {
    func loginTableCellHeight () -> CGFloat
    func userPressedSignUpButton ()
    func userPressedSignInButton (email : String, password : String, isRemembered : Bool)
    func userPressedFacebookLoginButton()
}

protocol registrationDelegate : class {
    func didFinishedRegistration(email : String, password : String)
    
}
