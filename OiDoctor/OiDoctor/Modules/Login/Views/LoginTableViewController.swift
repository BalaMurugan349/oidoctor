//
//  LoginTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography
import BonMot

class LoginTableViewController: UITableViewController,LoginViewModelOutput {
    
    private let viewModel: LoginViewModelInput
    private var arrayTableViewCells : [LoginTableViewCell] = []
    private let buttonRememberMe : UIButton = UIButton(type: UIButtonType.custom)
    private let tableHeaderHeight : CGFloat = UIDevice().screenHeight * 0.30
    let emailCell : LoginTableViewCell = LoginTableViewCell()
    let passwordCell : LoginTableViewCell = LoginTableViewCell()
    
    init(viewModel: LoginViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SETUP UI
    func setupView (){
        NotificationCenter.default.addObserver(self, selector: #selector(didChangedRegion), name: NSLocale.currentLocaleDidChangeNotification, object: nil)
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "Background".backgroundImageName())
        self.tableView.backgroundView = imgvw
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTableHeaderView()
        self.setupTableViewCells()
        self.setUpFooterView()
    }
    
    @objc func didChangedRegion (){
        print("Region changed")
    }
    //TABLE HEADER VIEW
    func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let imgvwLogo = UIImageView(image: #imageLiteral(resourceName: "OiDoctorText"))
        imgvwLogo.contentMode = .scaleAspectFit
        viewHeader.addSubview(imgvwLogo)
        constrain(imgvwLogo,viewHeader) { (imgvwLogo,viewHeader) in
            imgvwLogo.width == UIDevice().screenWidth/2 - 20
            imgvwLogo.center == viewHeader.center
            imgvwLogo.height == 40
        }
        
        let labelHeading : UILabel = UILabel()
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: UIDevice().screenType == .iPhone5 ? 30 : 35.0)
        labelHeading.textColor = UIColor.white
        labelHeading.text = "SIGN IN title".localize
        labelHeading.textAlignment = .center
        viewHeader.addSubview(labelHeading)
        constrain(labelHeading,imgvwLogo,viewHeader) { (labelHeading,imgvwLogo,viewHeader) in
            labelHeading.leading == viewHeader.leading + 30
            labelHeading.trailing == viewHeader.trailing - 30
            labelHeading.top == imgvwLogo.bottom + 20
            labelHeading.height == 50
        }
        
        
    }
    
    //TABLE VIEW CELLS
    func setupTableViewCells (){
        emailCell.setupWith(placeholder: "Email Address".localize, keyBoardType: UIKeyboardType.emailAddress, isSecureText: false, leftImage: #imageLiteral(resourceName: "Username"), pickerType: .None)
        arrayTableViewCells.append(emailCell)
        
        passwordCell.setupWith(placeholder: "Password".localize, keyBoardType: UIKeyboardType.default, isSecureText: true, leftImage: #imageLiteral(resourceName: "Password"), pickerType: .None)
        arrayTableViewCells.append(passwordCell)
        self.tableView.reloadData()
    }
    
    //TABLE FOOTER VIEW
    func setUpFooterView (){
        let height = UIDevice().screenHeight - (self.viewModel.loginTableCellHeight() * 2) - tableHeaderHeight - UIDevice().statusBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //REMEMBER ME BUTTON
        let buttonHeight = self.viewModel.loginTableCellHeight() - 20
        buttonRememberMe.configure(title: " remember me".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonRememberMe.setImage(#imageLiteral(resourceName: "CheckboxUnSelected"), for: UIControlState.normal)
        buttonRememberMe.setImage(#imageLiteral(resourceName: "CheckboxSelected"), for: UIControlState.selected)
        buttonRememberMe.contentHorizontalAlignment = .left
        buttonRememberMe.addTarget(self, action: #selector(onRememberMeButtonPressed(sender:)), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonRememberMe)
        constrain(buttonRememberMe,viewFooter) { (buttonRemember,view) in
            buttonRemember.top == view.top + 5
            buttonRemember.leading == view.leading + 35
            buttonRemember.trailing == view.trailing - 100
            buttonRemember.height == buttonHeight
        }
        let buttonQuestion = UIButton(type: UIButtonType.custom)
        buttonQuestion.setImage(#imageLiteral(resourceName: "QuestionMark"), for: UIControlState.normal)
        viewFooter.addSubview(buttonQuestion)
        constrain(buttonQuestion,viewFooter,buttonRememberMe) { (buttonQuestion,view,buttonRememberMe) in
            buttonQuestion.centerY == buttonRememberMe.centerY
            buttonQuestion.leading == view.trailing - 75
            buttonQuestion.width == 50
            buttonQuestion.height == 50
        }
        
        //SIGN IN BUTTON
        let topSpace = (height - (buttonHeight * 4))/4
        let buttonSignIn = UIButton(type: UIButtonType.custom)
        buttonSignIn.configure(title: "SIGN IN".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 18.0), backgroundColor: UIColor.clear)
        buttonSignIn.setBackgroundImage(#imageLiteral(resourceName: "ButtonBackground"), for: UIControlState.normal)
        buttonSignIn.addTarget(self, action: #selector(onSignInButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonSignIn)
        constrain(buttonSignIn,buttonRememberMe,viewFooter) { (buttonSignIn,buttonRemember,view) in
            buttonSignIn.top == buttonRemember.bottom + topSpace
            buttonSignIn.leading == view.leading + 30
            buttonSignIn.trailing == view.trailing - 30
            buttonSignIn.height == buttonHeight
        }
        
        //FACEBOOK SIGN IN BUTTON
        let buttonFacebook = UIButton(type: UIButtonType.custom)
        buttonFacebook.configure(title: "Sign In with Facebook".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 17.0), backgroundColor: UIColor.clear)
        buttonFacebook.setBackgroundImage(#imageLiteral(resourceName: "FacebookBackground"), for: UIControlState.normal)
        buttonFacebook.addTarget(self, action: #selector(onFacebookLoginButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonFacebook)
        constrain(buttonFacebook,buttonSignIn,viewFooter) { (buttonFacebook,buttonSignIn,view) in
            buttonFacebook.top == buttonSignIn.bottom + topSpace
            buttonFacebook.leading == view.leading + 30
            buttonFacebook.trailing == view.trailing - 30
            buttonFacebook.height == buttonHeight
        }
        
        //SIGNUP BUTTON
        let baseStyle = StringStyle(
            .color(.white),
            .font(Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 13.0)),
            .alignment(.center)
        )
        let signupTitle = NSMutableAttributedString()
        let signupColor = baseStyle.byAdding(.color(.orange))
        signupTitle.append("Don't have an account?  ".localize.styled(with: baseStyle))
        signupTitle.append("SIGN UP".localize.styled(with: signupColor))
        let buttonSignUp = UIButton(type: UIButtonType.custom)
        buttonSignUp.configure(title: "", titleColor: UIColor.white, font: UIFont.systemFont(ofSize: 15), backgroundColor: UIColor.clear)
        buttonSignUp.addTarget(self, action: #selector(onSignUpButtonPressed), for: UIControlEvents.touchUpInside)
        buttonSignUp.setAttributedTitle(signupTitle, for: UIControlState.normal)
        viewFooter.addSubview(buttonSignUp)
        constrain(buttonSignUp,buttonFacebook,viewFooter) { (buttonSignUp,buttonFacebook,view) in
            buttonSignUp.top == buttonFacebook.bottom + topSpace + 15
            buttonSignUp.leading == view.leading + 30
            buttonSignUp.trailing == view.trailing - 30
            buttonSignUp.height == buttonHeight
        }
        
        if let email = UserDefaults.standard.value(forKey: Constants.loggedEmail) as? String,
            let password = UserDefaults.standard.value(forKey: Constants.loggedPassword) as? String
        {
            emailCell.textfieldCustom.text = email
            passwordCell.textfieldCustom.text = password
            buttonRememberMe.isSelected = true
        }

    }
    
    //MARK:- BUTTON ACTION
    //SIGNUP BUTTON
    @objc func onSignUpButtonPressed (){
        self.viewModel.userPressedSignUpButton()
    }
    
    //SIGN IN BUTTON
    @objc func onSignInButtonPressed (){
        self.viewModel.userPressedSignInButton(email: emailCell.textfieldCustom.text!, password: passwordCell.textfieldCustom.text!, isRemembered: buttonRememberMe.isSelected)
    }
    
    //FACEBOOK SIGN I
    @objc func onFacebookLoginButtonPressed (){
        self.viewModel.userPressedFacebookLoginButton()
    }
    
    //REMEMBER ME BUTTON
    @objc func onRememberMeButtonPressed (sender : UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    //MARK:- UITABLEVIEW DELEGATE
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.loginTableCellHeight()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayTableViewCells[indexPath.row]
    }
    
}

extension LoginTableViewController : registrationDelegate{
    func didFinishedRegistration(email: String, password: String) {
        emailCell.textfieldCustom.text = email
        passwordCell.textfieldCustom.text = password
    }

}
