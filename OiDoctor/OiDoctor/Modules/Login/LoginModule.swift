//
//  LoginModule.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct LoginModule {
    
    static func build() -> LoginTableViewController {
        let wireframe = LoginWireframe()
        let viewModel = LoginViewModel(wireframe: wireframe)
        let viewController = LoginTableViewController(viewModel: viewModel)
        viewModel.view = viewController as LoginViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
