//
//  LoginWireframe.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class LoginWireframe {
    
    weak var viewController: LoginTableViewController!
    
    //REGISTRATION
    func gotoRegistrationPage (){
        let registerVC = RegisterModule.build(fromVC: viewController)
        viewController.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    //HOME
    func didLogin (){
        (UIApplication.shared.delegate as! AppDelegate).didLogin()
    }
    
    //FULL NAME PAGE
    func gotoFullNamePage (){
        let fullNameVC = RegisterModule.buildFullname()
        viewController.navigationController?.isNavigationBarHidden = false
        viewController.navigationController?.pushViewController(fullNameVC, animated: true)
    }

}
