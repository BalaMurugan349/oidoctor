//
//  LoginViewModel.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit

class LoginViewModel: LoginViewModelInput {
    
    private let wireframe: LoginWireframe
    weak var view: LoginViewModelOutput!
    private let loginManager = FBSDKLoginManager()
    
    func loginTableCellHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6:
            return 70
        case .iPhone6Plus,.iPhoneX:
            return 80
        default:
            return 60
        }
    }
    
    init(wireframe: LoginWireframe) {
        self.wireframe = wireframe
        
    }
    
    //SIGN UP
    func userPressedSignUpButton (){
        wireframe.gotoRegistrationPage()
    }
    
    //SIGN IN
    func userPressedSignInButton (email : String, password : String, isRemembered : Bool) {
        if email.isEmpty || !email.isValidEmail{
            self.view.showAlert(with: "Please enter the valid email".localize)
        }else if password.isEmpty{
            self.view.showAlert(with: "Please enter the password".localize)
        }else{
            SKActivityIndicator.show()
           // let locale = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"
            let loginParams : [String : Any] = ["locale" : localeSelected,
                                                "email" : email,
                                                "password" : password,
                                                "login_type" : "0",
                                                "access_token" : "",
                                                "social_id" : ""]
            self.loginWithApi(params: loginParams, isRemembered: isRemembered)
        }
    }
    
    //FACEBOOK LOGIN
    func userPressedFacebookLoginButton()  {
        loginManager.logIn(withReadPermissions: ["public_profile"], from: self.view as! UIViewController) { (result, error) in
            if error == nil && result?.isCancelled == false {
                SKActivityIndicator.show()
                let accessToken = result?.token.tokenString
                let parameter = ["fields":"id,name,email,first_name,last_name,location"]
                let _ = FBSDKGraphRequest(graphPath: "me", parameters: parameter)?.start(completionHandler: { (connection, result, error) in
                    if error == nil {
                        if let details = result as? [String: String] {
                            self.configureFacebookUserData(details: details, accessToken: accessToken)
                        }else {
                            SKActivityIndicator.dismiss()
                        }
                    }else{
                        SKActivityIndicator.dismiss()
                        self.view.showAlert(with: error!.localizedDescription)
                    }
                })
            }
        }
    }
    
    func configureFacebookUserData (details : [String : String], accessToken : String?){
        print("DETAILS \(details)")
        guard let fbID = details["id"],
            let token = accessToken else {
                self.view.showAlert(with: "Facebook Login Failed. Please try again".localize)
                return
        }
       // let locale = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"
        let loginParams : [String : Any] = ["locale" : localeSelected,
                                            "email" : "",
                                            "password" : "",
                                            "login_type" : "1",
                                            "access_token" : token,
                                            "social_id" : fbID]
        self.loginWithApi(params: loginParams, isRemembered: false)
        
    }
    
    //LOGIN WITH API
    func loginWithApi(params : [String : Any], isRemembered : Bool)  {
        print("Params = \(params)")
        ServiceManager.postDataFromService(baseurl: Constants.API.baseURL, serviceName: "login", params: params, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.showAlert(with: "Login Failed! Please try again".localize)
                }else{
                    //LOGIN SUCCESS
                    self.updateRememberCredentials(isRemembered: isRemembered, email: params["email"] as! String, password: params["password"] as! String)
                    LoggedUser.saveLoggedUserdetails(dictDetails: resultDict["data"] as! NSDictionary)
                    LoggedUser.shared.storeLoggedUserDetails(dict: resultDict["data"] as! NSDictionary)
                   //LoggedUser.shared.fullName == "nil" ? self.wireframe.gotoFullNamePage() : self.wireframe.didLogin()
                    self.wireframe.gotoFullNamePage()
                }
            }
            print(result)
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.showAlert(with: error.localizedDescription)
        }
    }
    
    func updateRememberCredentials (isRemembered : Bool, email : String, password : String){
        if isRemembered {
            UserDefaults.standard.set(email, forKey: Constants.loggedEmail)
            UserDefaults.standard.set(password, forKey: Constants.loggedPassword)
        }else{
            UserDefaults.standard.set(nil, forKey: Constants.loggedEmail)
            UserDefaults.standard.set(nil, forKey: Constants.loggedPassword)
        }
        UserDefaults.standard.synchronize()
    }
}
