//
//  SideMenuTableViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 2/8/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {

    private let arrayMenu : [String] = ["Logout".localize]
    
    enum sideMenu : Int {
        case logout
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        //self.tableView.register(UITableViewCell.class, forCellReuseIdentifier: "MenuCell")
        self.setupTableHeaderView()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: UIDevice().screenHeight - 80))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenu.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell")
        if cell == nil{
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "MenuCell")
        }
        cell?.selectionStyle = .none
        cell?.textLabel?.text = arrayMenu[indexPath.row]
        cell?.textLabel?.textAlignment = .center
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.logout()
            break
        default:
            break
        }
    }
    
    func logout (){
        let alertController = UIAlertController(title: "OiDoctor", message: "Are you sure you want to logout", preferredStyle: .alert)
        let alertActionNo = UIAlertAction(title: "No".localize, style: .destructive) { (action) in
        }
        alertController.addAction(alertActionNo)
        let alertActionYes = UIAlertAction(title: "Yes".localize, style: .default) { (action) in
            UserDefaults.standard.removeObject(forKey: Constants.loggedUserDetails)
            UserDefaults.standard.synchronize()
            (UIApplication.shared.delegate as! AppDelegate).gotoLoginPage()
        }
        alertController.addAction(alertActionYes)
        self.present(alertController, animated: true, completion: nil)
    }
}
