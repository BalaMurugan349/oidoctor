//
//  HomeInterfaces.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

protocol HomeViewModelOutput: class {
    
}

protocol HomeViewModelInput: class {
    
}
