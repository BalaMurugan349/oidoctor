//
//  HomeViewController.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,HomeViewModelOutput {

    private let viewModel: HomeViewModelInput

    init(viewModel: HomeViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //SETUP VIEW
    func setupView (){
        self.view.backgroundColor = UIColor.white
    }
    

}
