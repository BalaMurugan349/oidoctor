//
//  HomeViewModels.swift
//  OiDoctor
//
//  Created by Bala Murugan on 1/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

class HomeViewModel: HomeViewModelInput {
    
    private let wireframe: HomeWireframe
    weak var view: HomeViewModelOutput!
    
    
    init(wireframe: HomeWireframe) {
        self.wireframe = wireframe
        
    }
}
